from typing import List, Set, Tuple

Literal = int
Clause = List[Literal]
Model = Set[Literal]


class Formula:
    _last_variable = 1
    @staticmethod
    def get_new_variable():
        v = Formula._last_variable
        Formula._last_variable += 1
        return v

    @staticmethod
    def reset_last_variable():
        Formula._last_variable = 1

    def set_up_last_variable(self):
        raise NotImplementedError

    def cnf(self, clauses: List[Clause]) -> Tuple[Literal, List[Clause]]:
        raise NotImplementedError


class LiteralF(Formula):
    def __init__(self, lit: Literal):
        self.literal = lit

    def set_up_last_variable(self):
        Formula._last_variable = max(Formula._last_variable, self.literal + 1)

    def cnf(self, clauses: List[Clause]):
        return self.literal, clauses


class Not(Formula):
    def __init__(self, formula: Formula):
        self.formula = formula

    def set_up_last_variable(self):
        self.formula.set_up_last_variable()

    def cnf(self, clauses: List[Clause]):
        literal, clauses = self.formula.cnf(clauses)
        return -literal, clauses


class And(Formula):
    def __init__(self, left: Formula, right: Formula):
        self.left = left
        self.right = right

    def set_up_last_variable(self):
        self.left.set_up_last_variable()
        self.right.set_up_last_variable()

    def cnf(self, clauses: List[Clause]):
        l1, clauses = self.left.cnf(clauses)
        l2, clauses = self.right.cnf(clauses)
        p = Formula.get_new_variable()
        clauses.extend([[-p, l1], [-p, l2], [p, -l1, -l2]])
        return p, clauses


class Or(Formula):
    def __init__(self, left: Formula, right: Formula):
        self.left = left
        self.right = right

    def set_up_last_variable(self):
        self.left.set_up_last_variable()
        self.right.set_up_last_variable()

    def cnf(self, clauses: List[Clause]):
        l1, clauses = self.left.cnf(clauses)
        l2, clauses = self.right.cnf(clauses)
        p = Formula.get_new_variable()
        clauses.extend([[-p, l1, l2], [p, -l1], [p, -l2]])
        return p, clauses


def CNF(formula: Formula):
    formula.set_up_last_variable()
    l, clauses = formula.cnf([])
    clauses.append([l])
    Formula.reset_last_variable()
    return clauses


def update_model(M: Model, l: Literal):
    M = M.copy()
    M.add(l)
    return M


def EliminatePureLiteral(S: List[Clause], l: Literal) -> List[Clause]:
    def drop(clause: Clause):
        if l in clause:
            clause.remove(l)
        return clause
    return [drop(clause.copy()) for clause in S]


def UnitPropagate(S: List[Clause], l: Literal) -> List[Clause]:
    return EliminatePureLiteral([clause for clause in S if l not in clause], -l)


def PureLiterals(S: List[Clause]):
    lits = set()
    for clause in S:
        lits.update(clause)
    for lit in lits:
        if -lit not in lits:
            yield lit


def ChooseLiteral(S: List[Clause]) -> Literal:
    return S[0][0]


def DPLL(S: List[Clause], M: Model):
    modified = True
    while modified:
        modified = False
        for cl in S:
            if len(cl) == 1:
                S = UnitPropagate(S, cl[0])
                M = update_model(M, cl[0])
                modified = True
                break
    if [] in S: return None
    for l in PureLiterals(S):
        S = UnitPropagate(S, l)
        M = update_model(M, l)
    if not S: return M
    l = ChooseLiteral(S)

    tryPos = DPLL(S + [[l]], update_model(M, l))
    if tryPos:
        return tryPos
    return DPLL(S + [[-l]], update_model(M, -l))

DPLL1 = lambda S: DPLL(S, set())

if __name__ == "__main__":
    print(CNF(Not(And(LiteralF(1), Or(LiteralF(2), Not(LiteralF(3)))))))
    assert DPLL1([[1, 2], [-1], [-2]]) is None
    assert DPLL1([[1, 2]]) & {1, 2} != {}
    assert DPLL1([[-1, 2, 3], [1, 3, 4], [1, 3, -4], [1, -3, 4], [1, -3, -4], [-2, -3, 4], [-1, 2, -3], [-1, -2, 3]]) == {1, 2, 3, 4}
