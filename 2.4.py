from pysat.solvers import Glucose4
from pysat.card import CardEnc
from typing import List


ismine = lambda c: c == '*'
isfield = lambda c: c == '?'
isinfo = lambda c: c.isdecimal() and c != '9'


def validate(table: List[str]):
    n = len(table[0])
    assert all(len(line) == n for line in table), "Input is not a table!"
    assert all(all(ismine(c) or isfield(c) or isinfo(c) for c in line) for line in table), "Bad input format!"


def get_cells_around(table, center_i, center_j):
    mines = 0
    cells = []
    for i in range(max(center_i - 1, 0), min(len(table), center_i + 2)):
        for j in range(max(center_j - 1, 0), min(len(table[0]), center_j + 2)):
            if i != center_i or j != center_j:
                cell = table[i][j]
                if ismine(cell):
                    mines += 1
                elif isfield(cell):
                    cells.append((i, j))
    return mines, cells


class MinesTask:
    def __init__(self):
        self.last_var_number = 0
        self.namer = {}
        self.solver = Glucose4()

    def get_name(self, i: int, j: int):
        if i in self.namer:
            if j in self.namer[i]:
                return self.namer[i][j]
            else:
                self.last_var_number += 1
                self.namer[i][j] = self.last_var_number
        else:
            self.last_var_number += 1
            self.namer[i] = {j: self.last_var_number}
        return self.last_var_number

    def generate_assertion(self, table: List[str], i: int, j: int):
        mines, cells = get_cells_around(table, i, j)
        mines = int(table[i][j]) - mines
        names = [self.get_name(i, j) for i, j in cells]
        clauses = CardEnc.equals(names, bound=mines, top_id=self.last_var_number)
        self.last_var_number = clauses.nv + 1
        self.solver.append_formula(clauses.clauses)

    def check_configuration(self, table: str, row: int, column: int):
        table = table.strip().split('\n')
        validate(table)
        for i, line in enumerate(table):
            for j, cell in enumerate(line):
                if isinfo(cell):
                    self.generate_assertion(table, i, j)
        input_cell_can_have_mine = self.solver.solve([self.get_name(row, column)])  # has (row, column) cell a mine?
        return input_cell_can_have_mine


map1 = """\
****
2343
????
"""
map2 = """\
***
*4?
???
"""
unsafe_configurations = [
    (map1, 2, 3)
]
safe_configurations = [
    (map1, 2, 0),
    (map1, 2, 1),
    (map1, 2, 2),
    (map2, 1, 2),
    (map2, 2, 0),
    (map2, 2, 1),
    (map2, 2, 2)
]

for conf in unsafe_configurations:
    assert MinesTask().check_configuration(*conf)

for conf in safe_configurations:
    assert not MinesTask().check_configuration(*conf)
