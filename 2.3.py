"""
    INPUT: k : int
    OUTPUT: max { size of clique }
"""
from pysat.card import CardEnc
from pysat.solvers import Solver, SolverNames
from pysat.formula import CNFPlus
from threading import Timer


class CliqueTask:
    def __init__(self, k: int):
        self.colors = k
        self.graph = [[]]
        self.last_edge_code = 1
        self.solver = Solver(name=SolverNames.glucose4[0], use_timer=True)
        self.timer = None
        # self.solver = CNFPlus()
        # self.solver.append_formula = self.solver.extend

    def set_up_timer(self):
        self.timer = Timer(max(5, self.solver.time() * 4), lambda s: s.interrupt(), [self.solver])

    def add_edge(self):
        new_last = self.last_edge_code + self.colors
        edge = list(range(self.last_edge_code, new_last))
        c = CardEnc.equals(lits=edge, bound=1, top_id=new_last-1)  # An edge has exactly one color
        self.solver.append_formula(c.clauses)
        self.last_edge_code = c.nv + 1
        return edge

    def vertices(self):
        return len(self.graph)

    def add_triangle_rule_clauses(self, l_vertex_edge, r_vertex_edge, triangle_edge):
        self.solver.append_formula([-ei, -fi, -gi] for ei, fi, gi in zip(l_vertex_edge, r_vertex_edge, triangle_edge))

    def add_vertex(self):
        new_vertex = [self.add_edge() for _ in range(self.vertices())]
        for l_vertex_index, l_vertex_edge in enumerate(new_vertex):
            for r_vertex_index in range(l_vertex_index+1, self.vertices()):
                r_vertex_edge = new_vertex[r_vertex_index]
                triangle_edge = self.graph[r_vertex_index][l_vertex_index]
                self.add_triangle_rule_clauses(l_vertex_edge, r_vertex_edge, triangle_edge)
        self.graph.append(new_vertex)

    def solve(self):
        self.set_up_timer()
        self.timer.start()
        while self.solver.solve_limited():
            self.add_vertex()
            print("trying %d vertices (%d clauses with %d variables) last solved for %f sec" % (self.vertices(), self.solver.nof_clauses(), self.solver.nof_vars(), self.solver.time()))
            self.timer.cancel()
            self.set_up_timer()
            self.timer.start()
            # print(self.solver.get_model())
        self.solver.clear_interrupt()
        print("answer:", self.vertices() - 1)

    def test(self):
        for _ in range(16):
            self.add_vertex()
        print(self.vertices(), "vertices")
        self.solver.to_file("sth.sat")
        # print(self.solver.get_status())
        # print(self.solver.solve())

# CliqueTask(k=1).solve()
# CliqueTask(k=2).solve()
# CliqueTask(k=10).solve()()
CliqueTask(k=3).solve()
